package com.example.mockexam.models;

import com.google.gson.annotations.SerializedName;

public class ProfileModel {
    @SerializedName("is_profile_complete")
    private boolean isProfileComplete;
    @SerializedName("rating")
    private float rating;

    public boolean isProfileComplete() {
        return isProfileComplete;
    }

    public void setProfileComplete(boolean profileComplete) {
        isProfileComplete = profileComplete;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }
}

package com.example.mockexam.models;

import com.google.gson.annotations.SerializedName;

public class DataModel {
    @SerializedName("id")
    private int id;
    @SerializedName("value")
    private String value;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

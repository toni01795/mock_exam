package com.example.mockexam.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserModel {
    @SerializedName("message")
    private String message;
    @SerializedName("user_id")
    private int userId;
    @SerializedName("name")
    private String name;
    @SerializedName("email")
    private String email;
    @SerializedName("mobile")
    private long mobile;
    @SerializedName("profile_details")
    private ProfileModel profileDetails;
    @SerializedName("data_list")
    private List<DataModel> dataList;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getMobile() {
        return mobile;
    }

    public void setMobile(long mobile) {
        this.mobile = mobile;
    }

    public ProfileModel getProfileDetails() {
        return profileDetails;
    }

    public void setProfileDetails(ProfileModel profileDetails) {
        this.profileDetails = profileDetails;
    }

    public List<DataModel> getDataList() {
        return dataList;
    }

    public void setDataList(List<DataModel> dataList) {
        this.dataList = dataList;
    }
}

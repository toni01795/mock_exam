package com.example.mockexam;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.mockexam.models.UserModel;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    private final String TAG = getClass().getSimpleName();
    private TextView txtMessage;
    private ProgressBar progressCircular;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtMessage = findViewById(R.id.txt_message);
        progressCircular = findViewById(R.id.progress_circular);
        callApi();
    }

    private void callApi() {
        try {
            txtMessage.setText(getString(R.string.consuming_service));
            progressCircular.setVisibility(View.VISIBLE);
            ApiCallTask apiCallTask = new ApiCallTask();
            apiCallTask.start();
        } catch (Exception ex) {
            txtMessage.setText(getString(R.string.consumption_error));
            progressCircular.setVisibility(View.GONE);
        }
    }

    public class ApiCallTask extends Thread {

        @Override
        public void run() {
            super.run();

            String urlServicioWeb = "https://run.mocky.io/v3/7fd227c7-bd6f-4fdb-ba47-8b2176343036"; // URL del servicio web
            URL url = null;
            try {
                url = new URL(urlServicioWeb);
            } catch (MalformedURLException e) {
                e.printStackTrace();
                runOnUiThread(() -> {
                    txtMessage.setText(getString(R.string.consumption_error));
                    progressCircular.setVisibility(View.GONE);
                });
                return;
            }

            HttpURLConnection conexion = null;
            try {
                conexion = (HttpURLConnection) url.openConnection();
            } catch (IOException e) {
                e.printStackTrace();
                runOnUiThread(() -> {
                    txtMessage.setText(getString(R.string.consumption_error));
                    progressCircular.setVisibility(View.GONE);
                });
                return;
            }
            try {
                conexion.setRequestMethod("GET");
            } catch (ProtocolException e) {
                e.printStackTrace();
                runOnUiThread(() -> {
                    txtMessage.setText(getString(R.string.consumption_error));
                    progressCircular.setVisibility(View.GONE);
                });
                return;
            }

            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new InputStreamReader(conexion.getInputStream()));
            } catch (Exception e) {
                e.printStackTrace();
                runOnUiThread(() -> {
                    txtMessage.setText(getString(R.string.consumption_error));
                    progressCircular.setVisibility(View.GONE);
                });
                return;
            }
            StringBuilder responseJson = new StringBuilder();
            String line = "";
            while (true) {
                try {
                    if (!((line = reader.readLine()) != null)) break;
                } catch (IOException e) {
                    e.printStackTrace();
                    runOnUiThread(() -> {
                        txtMessage.setText(getString(R.string.consumption_error));
                        progressCircular.setVisibility(View.GONE);
                    });
                }
                responseJson.append(line);
            }
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
                runOnUiThread(() -> {
                    txtMessage.setText(getString(R.string.consumption_error));
                    progressCircular.setVisibility(View.GONE);
                });
                return;
            }

            String response = responseJson.toString();
            if(response.length() > 0 && !response.endsWith("}")) {
                responseJson.append("}");
            }

            // Parsear la respuesta JSON utilizando Gson
            Gson gson = new Gson();
            UserModel userModel = gson.fromJson(responseJson.toString(), UserModel.class);
            conexion.disconnect();

            runOnUiThread(() -> {
                txtMessage.setText(getString(R.string.finished_consumption));
                progressCircular.setVisibility(View.GONE);
            });
            Log.i(TAG, new Gson().toJson(userModel));
        }
    }
}